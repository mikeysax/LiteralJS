<p align="center">
  <img src="literalJS.png" alt="LiteralJS Logo">
</p>

---
# LiteralJS
### A small JavaScript library for building user interfaces.

### Links
- **Documentation**: https://literaljs.com

Recent 7.0.2 microbundle:
```js
Build "literaljs" to build:
       1856 B: index.js.gz
       1611 B: index.js.br
       1859 B: index.m.js.gz
       1620 B: index.m.js.br
       1919 B: index.umd.js.gz
       1680 B: index.umd.js.br
```
### [Features and stuff](#features)

- **Small**: Under 2kB in size (using microbundle).
- **Startup**: Amazing startup metrics; Great for weaker devices and weaker connections.
- **Simple**: LiteralJS uses plain functions and applies structure to building components for easier development.
- **Fast**: Current and previous vDOM data is diffed instead of the actual DOM for performant updates and rendering.
- **Virtual DOM**: Diffing occurs only on state update for more efficient DOM updates.
- **Flexible Syntax**: Freedom to use JSX, Hyperscript, or Object syntax.
- **Lifecycle Methods**: Components have the ability to trigger `mounted`, `updated`, and `unmounted` lifecycle functions.
- **Global Application Store**: One source of truth which makes other state management libraries (like Redux) less of a need.
- **Component State**: Manage state individually per component.
- **Event Delegation**: Uses event delegation for most events within the application for increased performance.
