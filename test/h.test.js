import { h } from '../src';

describe('H Tests', () => {
  test('H Node Created', () => {
    expect(h('div')).toEqual({
      e: 'div',
      a: {},
      c: []
    });
  });

  test('H Node Created With Single Child', () => {
    expect(h('div', {}, ['hello'])).toEqual({
      e: 'div',
      a: {},
      c: ['hello']
    });

    expect(h('div', {}, 'world')).toEqual({
      e: 'div',
      a: {},
      c: ['world']
    });
  });

  test('H Node Create With Multiple Children Of VNodes, Different Types, And Same Types', () => {
    expect(h('div', {}, ['foo', 'bar', 'baz'])).toEqual({
      e: 'div',
      a: {},
      c: ['foo', 'bar', 'baz']
    });

    expect(h('div', {}, 'foo', 'bar', 'baz')).toEqual({
      e: 'div',
      a: {},
      c: ['foo', 'bar', 'baz']
    });

    expect(h('div', {}, [0, 1, 2, 3, 4])).toEqual({
      e: 'div',
      a: {},
      c: [0, 1, 2, 3, 4]
    });

    expect(h('div', {}, 0, 1, 2, 3, 4)).toEqual({
      e: 'div',
      a: {},
      c: [0, 1, 2, 3, 4]
    });

    expect(h('div', {}, ['foo', 1, 'baz'])).toEqual({
      e: 'div',
      a: {},
      c: ['foo', 1, 'baz']
    });

    expect(h('div', {}, 'foo', 1, 'baz')).toEqual({
      e: 'div',
      a: {},
      c: ['foo', 1, 'baz']
    });

    expect(
      h('div', {}, [h('div', {}, 'hello'), h('div', {}, 'world')])
    ).toEqual({
      e: 'div',
      a: {},
      c: [
        {
          e: 'div',
          a: {},
          c: ['hello']
        },
        {
          e: 'div',
          a: {},
          c: ['world']
        }
      ]
    });

    expect(h('div', {}, h('div', {}, 'hello'), h('div', {}, 'world'))).toEqual({
      e: 'div',
      a: {},
      c: [
        {
          e: 'div',
          a: {},
          c: ['hello']
        },
        {
          e: 'div',
          a: {},
          c: ['world']
        }
      ]
    });

    expect(
      h('div', {}, [
        'foo',
        'bar',
        h('div', {}, 'hello'),
        1,
        2,
        h('div', {}, 'world')
      ])
    ).toEqual({
      e: 'div',
      a: {},
      c: [
        'foo',
        'bar',
        {
          e: 'div',
          a: {},
          c: ['hello']
        },
        1,
        2,
        {
          e: 'div',
          a: {},
          c: ['world']
        }
      ]
    });

    expect(
      h(
        'div',
        {},
        'foo',
        'bar',
        h('div', {}, 'hello'),
        1,
        2,
        h('div', {}, 'world')
      )
    ).toEqual({
      e: 'div',
      a: {},
      c: [
        'foo',
        'bar',
        {
          e: 'div',
          a: {},
          c: ['hello']
        },
        1,
        2,
        {
          e: 'div',
          a: {},
          c: ['world']
        }
      ]
    });
  });

  test('H Node With Attributes', () => {
    expect(
      h(
        'div',
        {
          class: 'container'
        },
        '!'
      )
    ).toEqual({
      e: 'div',
      a: {
        class: 'container'
      },
      c: ['!']
    });

    expect(
      h(
        'div',
        {
          class: 'container',
          id: 'foo'
        },
        1,
        2,
        3
      )
    ).toEqual({
      e: 'div',
      a: {
        class: 'container',
        id: 'foo'
      },
      c: [1, 2, 3]
    });
  });

  test('H Node Flattens Children', () => {
    expect(h('div', {}, [['foo'], ['bar'], ['baz']])).toEqual({
      e: 'div',
      a: {},
      c: ['foo', 'bar', 'baz']
    });
  });
});
