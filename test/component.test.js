import { component, h } from '../src';

describe('Component Tests', () => {
	test('Component H Syntax Works', () => {
		const Comp = new component({
			render() {
				return h('div', {}, h('span', {}, 'Hello World'));
			}
		})().init();

		expect(Comp.render()).toEqual({
			e: 'div',
			a: {},
			c: [
				{
					e: 'span',
					a: {},
					c: ['Hello World']
				}
			]
		});
	});

	test('Component Props Injection Works', () => {
		const count = 1;
		const Comp = new component({
			render() {
				return h('div', {}, h('span', {}, this.props.count));
			}
		})({ count: count }).init();

		expect(Comp.render()).toEqual({
			e: 'div',
			a: {},
			c: [
				{
					e: 'span',
					a: {},
					c: [count]
				}
			]
		});
	});

	test('Component State Injection Works', () => {
		const count = 1;
		const Comp = component({
			state: {
				count: count
			},
			render() {
				return h('div', {}, h('span', {}, this.getState().count));
			}
		})().init();

		expect(Comp.render()).toEqual({
			e: 'div',
			a: {},
			c: [
				{
					e: 'span',
					a: {},
					c: [count]
				}
			]
		});
	});

	test('Component Method Injection Works', () => {
		const count = 1;
		const Comp = component({
			methods() {
				return {
					count() {
						return count;
					}
				};
			},
			render() {
				return h('div', {}, h('span', {}, this.count()));
			}
		})().init();
		expect(Comp.render()).toEqual({
			e: 'div',
			a: {},
			c: [
				{
					e: 'span',
					a: {},
					c: [count]
				}
			]
		});
	});
});
