import { render, component, h } from '../src';

describe('Lifecycle Tests', () => {
	beforeEach(() => {
		document.body.innerHTML = '<div id="root"></div>';
	});

	test('Lifecycle mounted works with getState and setState', (done) => {
		const Comp2 = component({
			state: { show: false, count: 1 },
			mounted() {
				this.setState({ count: this.getState().count + 1 });
				const { count } = this.getState();
				expect(count).toBe(2);
				done();
			},
			render() {
				return h(
					'button',
					{
						id: 'button',
						events: {
							click: () => {
								this.setState({ show: !this.getState().show });
							}
						}
					},
					this.getState().show &&
					h('span', {}, [
						h('span', { id: 'count' }, this.getState().count)
					])
				);
			}
		});

		const Comp = component({
			render() {
				return h('div', { class: 'container' }, Comp2());
			}
		});

		render(Comp, 'root', {});

		document.getElementById('button').click();
	});

	test('Lifecycle unmounted works with getState and setState', (done) => {
		const Comp2 = component({
			state: { count: 1 },
			unmounted() {
				expect(this.getState().count).toBe(1);
				done();
			},
			render() {
				return h(
					'div',
					{ },
					h('span', {}), [
						h('span', { id: 'count' }, this.getState().count)
					]
				);
			}
		});

		const Comp = component({
			state: { show: true },
			mounted() {
				this.setState({ show: !this.getState().show });
			},
			render() {
				return h('div', { class: 'container' }, [
					this.getState().show && Comp2()
				]);
			}
		});

		render(Comp, 'root', {});
	});

	test('Lifecycle updated works with getState and setState', (done) => {
		const Comp2 = component({
			state: { count: 1 },
			mounted() {
				this.setState((state) => ({ count: state.count + 1 }));
			},
			updated() {
				expect(this.getState().count).toBe(2);
				done();
			},
			render() {
				return h(
					'div',
					{ },
					h('span', {}), [
						h('span', { id: 'count' }, this.getState().count)
					]
				);
			}
		});

		const Comp = component({
			state: { show: false },
			mounted() {
				this.setState({ show: !this.getState().show });
			},
			render() {
				return h('div', { class: 'container' }, [
					this.getState().show && Comp2()
				]);
			}
		});

		render(Comp, 'root', {});
	});
});
