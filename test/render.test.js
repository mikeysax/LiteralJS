import { render, component, h } from '../src';

describe('Render Tests', () => {
	beforeAll((done) => {
		done();
	});

	test('Render Mounts Properly', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp = component({
			render() {
				return h('div', { id: 'container' }, 'Hello World');
			}
		});

		render(Comp, 'root', {});

		const containerHTML = document.getElementById('container').innerHTML;

		expect(containerHTML).toEqual('Hello World');
	});

	test('Render Global Store Properly', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp = component({
			render() {
				return h(
					'span',
					{ class: 'container', id: 'count' },
					this.getStore().count
				);
			}
		});

		render(Comp, 'root', { count: 1 });

		const count = document.getElementById('count').innerHTML;

		expect(count).toEqual('1');
	});

	test('Render Multiple Components With State Properly', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp2 = component({
			state: { count: 1 },
			render() {
				return h('span', { id: 'count' }, this.getState().count);
			}
		});

		const Comp = component({
			render() {
				return h('div', { class: 'container' }, Comp2());
			}
		});

		render(Comp, 'root', {});

		const count = document.getElementById('count').innerHTML;

		expect(count).toEqual('1');
	});

	test('Renders True Markup Booleans Properly', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp = component({
			state: { show: true },
			render() {
				return h('div', { class: 'container' }, [
					this.getState().show && h('span', { id: 'hello' }, 'Hello World')
				]);
			}
		});

		render(Comp, 'root', {});

		const containerHTML = document.getElementById('hello').innerHTML;

		expect(containerHTML).toEqual('Hello World');
	});

	test('Renders False Markup Booleans Properly', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp = component({
			state: { show: false },
			render() {
				return h('div', { class: 'container' }, [
					this.getState().show &&
						h('span', { id: 'hello' }, 'Hello World')
				]);
			}
		});

		render(Comp, 'root', {});

		const element = document.getElementById('hello');

		expect(element).toBe(null);
	});

	test('getState and setState Works With Methods', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp2 = component({
			state: { count: 1 },
			methods() {
				return {
					increment() {
						this.setState((state) => ({ count: state.count + 1 }));
					},
					currentCount() {
						return this.getState().count;
					}
				};
			},
			render() {
				return h(
					'button',
					{
						id: 'button',
						events: {
							click: () => {
								this.increment();
								setTimeout(() => {
									const countHTML = document.getElementById('count').innerHTML;
									expect(countHTML).toBe('2');
									done();
								});
							},
						},
					},
					[
						'Click Me',
						h('span', { id: 'count' }, this.currentCount())
					]
				);
			}
		});

		const Comp = component({
			render() {
				return h('div', { class: 'container' }, Comp2());
			}
		});

		render(Comp, 'root', {});

		document.getElementById('button').click();
	});

	test('getState and setState Works In Render Event', () => {
		document.body.innerHTML = '<div id="root"></div>';

		const Comp2 = component({
			state: { count: 1 },
			render() {
				return h(
					'button',
					{
						id: 'button',
						events: {
							click: () => {
								this.setState((state) => ({ count: state.count + 1 }));
								setTimeout(() => {
									const countHTML = document.getElementById('count').innerHTML;
									expect(countHTML).toBe('2');
									done();
								});
							},
						},
					},
					[
						'Click Me',
						h('span', { id: 'count' }, this.getState().count),
					]
				);
			},
		});

		const Comp = component({
			render() {
				return h('div', { class: 'container' }, Comp2());
			},
		});

		render(Comp, 'root', {});

		document.getElementById('button').click();
	});
});
